# OlympicProject
## Olympics Software

El proyecto tiene como principal la rama master donde se encuentra el proyecto completo, en la versión más actualizada. La aplicación Web
desarrollada permite al usuario visualizar la información de los deportistas olimpicos. La aplicación permite realizar las operaciones 
de consulta, listado, eliminación, modificación y actualización de los datos.
A continuación se describe la estructra de carpetas. 
En la rama master tiene: 
- Primer nivel la carpeta se encuentra la carpeta olímpico 
- El README donde se encuentra la explicación del proyecto 
- La licencia con la que se trabajo en el poryecto

Dentro de la carpeta WebContent se encuentra: 
--META-INF: Donde se encuentra la vesión del manifiesto
--WEB-INF: Se ecuentra el beans version 
--Img: Donde se encuentra la imagen que hicimos uso para la pantalla principal de nuestro proyecto
--Css: Complementos para el uso de Bootstrap
--Js: Complementos para el uso de Bootstrap

### Pre requisitos

Tener instalado:
Eclipse  IDE for Enterprise Java Developers 
Postgres
Widfly

### Instalación
Este proyecto puede ser descargado y ejecutado en Eclipse una vez que haya sido configurado la base de datos y widfly.

# Colaboradores
## Grupo 2:
#### Almeida Edison 
#### Reina Miguel 
#### Sanmartín Alisson
