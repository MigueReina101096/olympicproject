<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Olimpiadas</title>
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="css/estilos.css" rel="stylesheet" type="text/css">
</head>
<body>
	<jsp:include page="head.jsp"></jsp:include>
	<form action="">
		<div class="container">
			<div class="row">
				<div class="col-sm">
					<label style="color: white;"> Elija un deporte: </label>
				</div>
				<div class="col-sm">
					<select class="form-control">
						<option>Seleccione un deporte</option>
						<option>Ciclismo</option>
					</select>
				</div>
			</div>
			<br/>
			<br/>
			<div class="row">
				<div class="col-sm"></div>
				<div class="col-sm">
					<a href='http://localhost:8080/olympic/Listar' class="btn btn-primary btn-block">Buscar</a>
				</div>
			</div>
		</div>
	</form>
	<jsp:include page="foot.jsp"></jsp:include>
</body>
</html>