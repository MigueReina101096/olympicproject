package epn.controlador;

import java.io.IOException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;

import epn.modelo.Deportista;

/**
 * @author Miguel Reina - Alisson Sanmart�n - Edison Almeida
 * Servlet implementation class Eliminar
 */

@Transactional
@WebServlet("/Eliminar")
public class Eliminar extends HttpServlet{
	
	private static final long serialVersionUID =1L;
	
	@PersistenceContext(unitName="olympicPU")
	private EntityManager em;
	
	public Eliminar() {
		
		super();
		
	}
	/**
     * doGet que sirve para eliminar datos
     * @param request - 
     * @param respone - 
     */
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		//Query q = em.createQuery("DELETE FROM Deportista d WHERE d.id=:id", Deportista.class);
		//q.setParameter("id", 1).executeUpdate();
		//em.getTransaction().commit();
		String id = req.getParameter("id");
		
		Deportista d = em.find(Deportista.class, Integer.parseInt(id));
		em.remove(d);
		
		req.getRequestDispatcher("Listar").forward(req, resp);
	
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		doGet(req, resp);
		
	}	
	
}
